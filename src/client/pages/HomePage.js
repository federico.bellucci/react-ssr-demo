import React from 'react'

const Home = () => {
    return (
        <div>
            <h3>Welcome</h3>
            <button onClick={() => { console.log("Hi there!") }}>Press me</button>
        </div>
    )
}

export default Home;