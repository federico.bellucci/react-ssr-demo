// startup point for the client application
import React from 'react'
import ReactDOM from 'react-dom'
import HomePage from '../client/pages/HomePage'

ReactDOM.render(
    <HomePage/>
    , document.querySelector('#root'))