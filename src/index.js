import React from 'react'
import express from 'express'
import { renderToString } from 'react-dom/server'
import HomePage from './client/pages/HomePage'
const app = express()

// we tell express that the public folder must be available to the outside as a static resource
app.use(express.static("public"))
app.get('*', (req, res) => {
    const content = renderToString(<HomePage/>)

    res.send(content)
})

app.listen(3000, () => {
    console.log("listening on port 3000")
})