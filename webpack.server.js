const path = require('path')
const merge = require('webpack-merge')
const baseConfig = require('./webpack.base')
const webpackNodeExternals = require('webpack-node-externals')

const config = {
    // Inform webpack that we are building a bundle for NodeJS, rather than for the browser
    target: 'node',

    // Tell Webpack the root file of our server application
    entry: './src/index.js',

    // Tell webpack where to put the output file that is genrated
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'build')
    },

    // exclude node_modules folder from bundle because in Node libraries are loaded dynamically
    externals: [webpackNodeExternals()]
}

module.exports = merge(baseConfig, config)